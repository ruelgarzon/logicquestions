<?php

declare(strict_types = 1);

class SmokingCigarette
{

    public function displayResult()
    {
        $testData =[3,5];
        foreach ($testData as $cig) {
            $totalCigarette = $this->_computeButts($cig,$cig);
            echo $totalCigarette;
            echo "<br>";
        }
    }

    // check and compute until the lowest possible cigarette to butt convertion is reached
    private function _computeButts(float $totalCigarette,float $newCigarette):float
    {
        $compCigarette = floor($newCigarette / 2);
        $totalCigarette += $compCigarette;
        $newCigarette = $compCigarette + ($newCigarette % 2);

        if ($newCigarette > 1) {
            $totalCigarette = $this->_computeButts($totalCigarette,$newCigarette);
        }
        return $totalCigarette;
    }
}
$SmokingCigarette = new SmokingCigarette();
$SmokingCigarette->displayResult();
