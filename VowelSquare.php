<?php

declare(strict_types = 1);

class VowelSquare
{

    public function displayResult()
    {
        $vowels = ["a","e","i","o","u"];
        $dataSet = ["abcd","eikr","oufj"];
        $outputDataSet = [];
        foreach ($dataSet as $setKey => $setValue) {
            $dataCluster = str_split($setValue);
            foreach ($dataCluster as $cluster_key => $clusterValue) {
                $outputDataSet[$setKey][$cluster_key] = in_array($clusterValue, $vowels);
            }
        }
        $this->_check_data($outputDataSet);
        
    } 

    // check and display if data set provided has a vowel square
    private function _check_data(array $outputDataSet)
    {
        $no_data = true;
        foreach ($outputDataSet as $setKey => $setValue) {
            foreach ($setValue as $valKey => $value) {
                if ($value == true) {
                    $top_right = (isset($outputDataSet[$setKey][$valKey + 1]) 
                        AND $outputDataSet[$setKey][$valKey + 1]) ? true : false;
                    $bot_left = (isset($outputDataSet[$setKey + 1][$valKey])
                        AND $outputDataSet[$setKey + 1][$valKey]) ? true : false;
                    $bot_right = (isset($outputDataSet[$setKey + 1][$valKey + 1])
                        AND $outputDataSet[$setKey + 1][$valKey + 1]) ? true : false;
                    if ($top_right AND $bot_left AND $bot_right) {
                        $no_data = false;
                        echo $setKey."-".$valKey;
                        break 2;
                    }
                }
            }
        }
        if ($no_data) {
            echo "not found";
        }
    }
}

$VowelSquare = new VowelSquare();
$VowelSquare->displayResult();