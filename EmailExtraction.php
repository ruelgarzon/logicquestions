<?php

declare(strict_types = 1);

class EmailExtraction
{

    public function displayResult()
    {
        $testData =["someone@sample.com",
                    "iam.a.test@sample.com"];
        foreach ($testData as $email) {
            $arrayMail = explode(".", $email);
            array_pop($arrayMail);
            if (count($arrayMail) == 1) {
                echo explode("@", $arrayMail[0])[0];
            } else {
                foreach($arrayMail as $val) {
                    echo $val[0];
                }
            }
            echo "<br>";
        }
    }
}
$EmailExtraction = new EmailExtraction();
$EmailExtraction->displayResult();
