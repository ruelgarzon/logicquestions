<?php

declare(strict_types = 1);

class FirstLetter
{

    public function displayResult()
    {
        $dataSet = ["aasdvasvavda","sajhags"];
        foreach ($dataSet as $value) {
            $newDataSet = str_split($value);
            $oldLetters = [];
            foreach ($newDataSet as $letter) {
                $notExisting = true;
                foreach ($oldLetters as $oldLetter) {
                    if($oldLetter == $letter)
                        $notExisting = false;
                }
                if ($notExisting) {
                    echo $letter;
                    $oldLetters[] = $letter;
                }
            }
            echo "<br>";
        }
    }
}
$FirstLetter = new FirstLetter();
$FirstLetter->displayResult();
