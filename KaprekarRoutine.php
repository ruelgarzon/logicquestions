<?php

declare(strict_types = 1);

class KaprekarRoutine
{

    public function displayResult()
    {
        $testData = [2, 3524, 1234];
        foreach ($testData as $value) {
            $this->_constructData($value, 0);
        }
    }

    // compute and count the given amount until the returned value reached 6174 or 0
    private function _constructData(float $amount, int $counter)
    {
        $counter++;
        $arrAmount = str_split((string) $amount);
        sort($arrAmount);
        $ascAmount = implode("", $arrAmount);

        rsort($arrAmount);
        $descAmount = implode("", $arrAmount);

        $resultAmount = $descAmount - $ascAmount;

        switch ($resultAmount) {
            case 0:
                echo "0";
                echo "<br>";
                break;
            case 6174:
                echo $counter;
                echo "<br>";
                break;
            
            default:
                $str_cnt = 4 - strlen((string) $resultAmount);
                if ($str_cnt > 0) {
                    $resultAmount = str_pad($resultAmount,$str_cnt + 1,"0",STR_PAD_LEFT);
                }
                $this->_constructData($resultAmount,$counter);
                break;
        }
    }
}
$KaprekarRoutine = new KaprekarRoutine();
$KaprekarRoutine->displayResult();
