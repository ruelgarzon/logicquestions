# SQL Problem 1
SELECT 
    first_name, 
    last_name, 
    middle_name,
    birth_date,
    CURDATE() AS date_today,
    TIMESTAMPDIFF(YEAR,
        birth_date,
        CURDATE()) AS age
FROM
    employees
HAVING age BETWEEN 31 AND 39;

# SQL Problem 2
SELECT 
    B.id as mst_therapist_id, 
    A.target_date, 
    A.start_time, 
    A.end_time ,
    CASE
        WHEN A.start_time <= "05:59:59" AND A.start_time >= "00:00:00" 
        THEN CONCAT(DATE_ADD(A.target_date, INTERVAL 1 DAY),' ',A.start_time)
    ELSE CONCAT(A.target_date,' ',A.start_time)
    END as sort_start_time
FROM daily_work_shifts A
INNER JOIN therapists B ON A.therapist_id = B.id
ORDER  BY target_date ASC ,sort_start_time ASC;

# SQL Problem 3
SELECT 
    A.id as mst_employee_id,
    A.first_name,
    A.last_name,
    A.birth_date,
    A.hire_date,
    CASE
        WHEN C.name = "CEO" THEN "Chief Executive Officer"
        WHEN C.name = "CTO" THEN "Chief Technical Officer"
        WHEN C.name = "CFO" THEN "Chief Financial Officer"
        ELSE C.name
    END as position_name
FROM employees A
JOIN employee_positions B ON A.id = B.employee_id
JOIN positions C ON B.position_id = C.id

# SQL Problem 4
SELECT 1 as id , null as parent_id
UNION SELECT 2 , 5
UNION SELECT 3 , null
UNION SELECT 4 , 1
UNION SELECT 5, null
UNION SELECT 6, 3
UNION SELECT 7, 3
ORDER BY IFNULL(parent_id,id)