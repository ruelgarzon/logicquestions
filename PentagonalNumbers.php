<?php

declare(strict_types = 1);

class PentagonalNumbers
{

    public function displayResult()
    {
        $feedNumber = 5;
        
        for ($cnt = 1; $cnt <= $feedNumber; $cnt++)
        {

            $number = (3 * ($cnt * $cnt) - $cnt) / 2;
            echo $number;
            echo "<br>";
        }
    } 
}
$PentagonalNumbers = new PentagonalNumbers();
$PentagonalNumbers->displayResult();