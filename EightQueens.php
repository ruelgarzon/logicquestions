<?php

declare(strict_types = 1);

class EightQueens
{
    
    private $board = [];
    
    public function displayResult()
    {
        $testData = [[1, 8, 2, 5, 3, 7, 4, 6],
                        [3, 5, 2, 8, 1, 7, 4, 6],
                        [1, 2, 3, 4, 5, 6, 7, 8]
                    ];
        foreach ($testData as $key => $testSet) {
            $this->_constructBoard($testSet);
            $displayText = "Yes";
            foreach ($testSet as $setKey => $value) {
                if ( $this->_check($setKey + 1, $value) === false) {
                    $displayText = "No";
                }
            }
            echo $displayText;
            echo "<br>";
        }
    }

    // check whether or the queen conflicts other queens' positions
    private function _check(int $x, int $y):bool
    {
        $n = 8;
        for ($i = 0; $i < $x; $i++)
        {
            // test the column to check for another Queen
            if (isset($this->board[$i][$y]) && $this->board[$i][$y] === 1) {
                return false;
            }
            // Test the diagonals (backwards from the coordinate)
            $tx = $x - 1 - $i;
            $ty = $y - 1 - $i;  // diagonal this way \
            if (($ty >= 0) && isset($this->board[$tx][$ty]) &&($this->board[$tx][$ty] === 1)) {
                return false;
            }
            $ty = $y + 1 + $i;  // diagonal this way /
            if (($ty < $n) && isset($this->board[$tx][$ty]) &&($this->board[$tx][$ty] === 1)) {
                return false;
            }
        }
        
        return true;
    }
    
    // initiate/place dataset in the board as an array
    private function _constructBoard(array $testSet)
    {
        foreach ($testSet as $key => $value) {
            $cnt = $key + 1;
            for ($i = 1; $i <= 8; $i++) { 
                $this->board[$cnt][$i] = ($i == $value) ? 1 : 0;
                $echo  = ($i == $value) ? " Y " : " N ";
            }
        }
    }
}
$EightQueens = new EightQueens();
$EightQueens->displayResult();
