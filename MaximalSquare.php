<?php

declare(strict_types = 1);

class MaximalSquare
{

    public function displayResult()
    {
        $row = 4;
        $column = 5;
        $data =
        [
            [1, 0, 1, 0, 0],
            [1, 0, 1, 1, 1],
            [1, 1, 1, 1, 1],
            [1, 0, 0, 1, 0]
        ];
        
        $maxCnt = $this->_constructData($data,$row,$column);
        echo $maxCnt * $maxCnt;
    }

    // get the maximun square value
    private function _constructData(array $data,int $row,int $column):int
    {
        $max = 0;
        for ($i = 0; $i < $row; $i++)
        {
            for ($j = 0; $j < $column; $j++)
            {

                $lookup[$i][$j] = $data[$i][$j];

                if ($i AND $j AND $data[$i][$j]) {
                    $lookup[$i][$j] = min (min ($lookup[$i][$j - 1], $lookup[$i - 1][$j]),
                                        $lookup[$i - 1][$j - 1]) + 1;
                }

                if ($max < $lookup[$i][$j]) {
                    $max = $lookup[$i][$j];
                }
            }
        }
        return $max;
    }
}
$MaximalSquare = new MaximalSquare();
$MaximalSquare->displayResult();
